<!DOCTYPE HTML>
<!-- need to split this up so it is retrieving the footer\sidebar\etc dynamically-->
<!-- possibly pull something together to grab the latest shortlinks I have made, probably involve some api work -->
<?php $info = json_decode(file_get_contents("./info.json")); ?>
<html>
	<head>
		<meta charset="utf-8">
		<!--need to change this back to having slash at front-->
		<link rel="stylesheet" type = "text/css" href="stylesheet.css"/>
		<link rel="stylesheet" type = "text/css" href="avatar-stylesheet.css"/>
		<link rel="icon" type="image/png" href="http://soliloquyforthefallen.net/resources/icon.png" />
		<!--[if IE]>
			<script src="/resource/js/html5.js"></script>
		<![endif]-->

		<title>Avatar Hall</title>
	</head>

	<body>
		<p>This is the Avatar Hall for</p>
		<p class="handle"><?php echo "$info->handle";?></p>
		<p><?php echo "$info->bio";?></p>
		<hr/><hr/>
		<div class="hall-container">
			<?php
				$filename = glob('avatars/'.'*.{png,jpg}', GLOB_BRACE);
				arsort($filename);
				foreach ($filename as $file) {
					if (file_exists($file.".txt"))
						$tag=file_get_contents($file.".txt");
					else
						$tag='';
					echo "<div class='avatar'><a href=avatars/".basename($file)."><img class='avatar' src='avatars/".basename($file)."'></a></div><div class='tag'><p>".$tag."</p></div>\n<hr class=hall>\n";
				}
			?>

		</div>

	</body>
	<footer class="centered">
		<p> You can find me at the following locations:</p>
		<p>
			<?php if ($info->social->twitter) { echo "<a href=http://twitter.com/".$info->social->twitter."/><img class='socialLogos' src='assets/Twitter_logo_blue.png'></a>";} ?>
			<?php if ($info->social->bitbucket) { echo "<a href=http://bitbucket.com/".$info->social->bitbucket."/><img class='socialLogos' src='assets/logoBitBucketPNG.png'></a>";} ?>
			<?php if ($info->social->github) { echo "<a href=http://github.com/".$info->social->github."/><img src='assets/GitHub-Mark-32px.png'></a>";} ?>
			<?php if ($info->social->facebook) { echo "<a href=http://facebook.com/".$info->social->facebook."/><img src='assets/FB-f-Logo__blue_50.png'></a>";} ?>
			<?php if ($info->social->keybase) { echo "<a href=http://keybase.io/".$info->social->keybase."/><img class='socialLogos' src='assets/keybase-logo.jpg'></a>";} ?>
		</p>
		<a href="https://bitbucket.org/oliverkrystal/avatar-hall">Avatar Hall</a> is software for showing off your avatars, past and present.  It was written by @oliverkrystal.
	</footer>
</html>
