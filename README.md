"Avatar Hall
Avatar Hall is meant to display your current and previous avatars.

#Install
Clone the repo to your webserver.  In the folder called "avatars" put your avatars and a json file.  Avatar Hall will dynamically pull the avatar and the infor from the json file and display it.

#JSON Format for your description, etc
This file should reside next to the index.php and be called "info.json"
{
    "handle": "oliverkrystal",
    "bio": "Its me! oliverkrystal",
    "social": {
        "twitter": "oliverkrystal",
        "github": "oliverkrystal",
        "facebook": "meh"
    }
}


#License
My code is meh.  Use as you like.
The web fonts are from here: http://www.fontsquirrel.com/fonts/junction-regular
